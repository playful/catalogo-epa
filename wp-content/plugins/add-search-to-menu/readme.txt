=== Ivory Search - WordPress Search Plugin ===
Contributors: ivorysearch, vinod dalvi, jack-kitterhing
Donate link: https://ivorysearch.com/contact/
Tags: search, search menu, woocommerce search, search plugin, search shortcode, search widget, exclude from search
Requires at least: 3.9
Tested up to: 4.9
Requires PHP: 5.2.4
Stable tag: 4.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Enhances WordPress search to power your site. Ivory Search provides you the options & tools that you need to create an advanced powerful search for your site.

== Description ==

Ivory Search enhances WordPress search to power your site. It provides various options & tools that you need to create an advanced powerful search for your site.

With Ivory Search, you can create unlimited number of search forms and configure each of them to search different content or exclude specific content from search.

Display the search forms in site header, footer, navigation menu or anywhere on the site using widget and shortcodes.

Read [this documentation](https://ivorysearch.com/knowledge-base/how-to-use-ivory-search-plugin/) to know how to use Ivory Search plugin.

= Key Features =
* Create unlimited number of search forms and configure them separately.
* Configure search form to search specific content.
* Configure search form to exclude specific content from search.
* Display the search forms anywhere on the site.
* Search all or any of the searched terms.
* Search whole or partial word of the searched terms using Fuzzy Matching.
* Search WooCommerce products and bbPress forums.

= Search Specific or Any Content =
* Search posts, pages, products, attachments, forums, media or any custom post types.
* Search in post title, post content and post excerpt.
* Search posts having specific categories, terms, tags, post formats, product category, product type or any custom taxonomies.
* Search posts having any or all the specified taxonomy terms (Pro).
* Search in taxonomy terms title and description.
* Search posts having specific custom fields or metadata.
* Search WooCommerce products SKU (Pro).
* Search specific author posts (Pro).
* Search in author Display name and display the posts created by that author.
* Search posts having specific post status (Pro).
* Search posts having specific, greater than or less than number of comments (Pro).
* Search in approved comments content.
* Search posts created before, after or in between specified dates.
* Search all posts with and without passwords or only posts with passwords or only posts without passwords.
* Search images, attachments and media having specific file type or MIME type (Pro).
* Search in title, caption and description of images, attachments and media (Pro).
* Search exact words with quotes, for example "search phrase".

= Exclude Specific Content from Search =
* Exclude from search specific posts, pages, products, attachments, forums or any custom posts.
* Do not search in post title, post content or post excerpt.
* Exclude posts from search having specific categories, terms, tags, post formats, product category, product type or any custom taxonomies.
* Exclude posts from search having specific custom fields or metadata (Pro).
* Exclude 'out of stock' WooCommerce products from search (Pro).
* Exclude specific author posts from search (Pro).
* Exclude posts from search having specific post status (Pro).
* Exclude sticky posts from search.
* Exclude images, attachments and media from search having specific file type or MIME type (Pro).
* Exclude posts from search having specific number of comments.
* Exclude posts from search created before or after specified dates.
* Do not search posts with passwords.

= Control and Extend Search =
* Add search to menu and configure its functionality and style.
* Configure number of posts to display on search results page.
* Order search results posts in ascending or descending order of date, relevance, id, author, title, type, name, comment count, menu order or randomly (Pro).
* Highlight searched terms on search results page.
* Option to display or not display sticky posts to the start of the search results page.
* Display an error for empty search query or display all posts.
* Option to search or not search post types which are excluded from search using exclude_from_search.
* Display search form only for site administrator for testing purpose before making it live.
* Disable specific search form or whole search functionality on the site.
* Search base word of searched keyword using Keyword Stemming (Pro).
* Option to add words to the list of stopwords. The stopwords will not be searched.
* Option to add synonyms to make the searches find better results.


The styling of search form highly depends on how the theme styles it so if you face any search form styling related issue then feel free to get free support from [Ivory Search support](https://ivorysearch.com/support/)

Ivory Search plugin was previously known as Add Search To Menu plugin.

= Premium =

This is the free version of Ivory Search. There is also Ivory Search Premium, which has added features marked as (Pro) above. For more information about Ivory Search Premium, see [IvorySearch.com](https://ivorysearch.com/).

= Help =

Need Help? Get [Ivory Search support](https://ivorysearch.com/support/).

= Feedback =

Any suggestions or comments are welcome. Feel free to contact us using this [Contact form](https://ivorysearch.com/contact/).

== Installation ==

* Install the plugin from the 'Plugins' section in your dashboard (Go to `Plugins -> Add New -> Search` and search for Ivory Search).
* Alternatively, you can [download](https://downloads.wordpress.org/plugin/add-search-to-menu.zip "Download Ivory Search") the plugin from the repository. Unzip it and upload it to the plugins folder of your WordPress installation (`wp-content/plugins/` directory of your WordPress installation).
* Activate it through the 'Plugins' section.
* Use the `Ivory Search` menu to configure the plugin.

== Frequently Asked Questions ==

= How to use Ivory Search plugin? =

Please read below documentation to know how to use Ivory Search plugin.

https://ivorysearch.com/knowledge-base/how-to-use-ivory-search-plugin/

== Screenshots ==

1. Includes section controls what content on the site to make searchable for the search form.
2. Excludes section controls what content on the site to exclude from search for the search form.
3. Search form settings section customizes overall behavior of the search form.
4. Ivory Search plugin options in the admin menu.
5. Search To Menu section provides options to customize the behavior of search form added in the site navigation menu.
6. Settings section of the settings page provides options to configure sitewide search functionality that affects all search forms.
7. Displays default search form in the menu.
8. Displays dropdown search form in the menu.
9. Displays sliding search form in the menu.
10. Displays full width search form in the menu.

== Changelog ==

= 4.0 =
* Renamed plugin from "Add Search To Menu" to "Ivory Search".
* Changed old settings page from Settings -> Add Search To Menu to Ivory Search -> Settings
* Added new Popup menu search form style.
* Developed option to created unlimited search forms and each search forms can be configured separately.
* Search specific post types, categories, taxonomies, terms, custom fields, authors, comments and many more.
* Exclude from search specific posts, pages, categories, taxonomies, terms, custom fields, authors, post status and many more.
* Developed options for searched posts ordering, Terms Highlighting, Fuzzy Matching, Keyword Stemming and many more.

= 3.4 =
* Resolved "PHP Notice: Undefined index: add_search_to_menu_style"

= 3.3 =
* Enhanced Mobile Display option to work with caching.

= 3.2 =
* Fixed incorrect JavaScript and CSS files URLs displayed in the plugin settings.
* Fixed issue of using quotation in plugin custom CSS option.

= 3.1 =
* Resolved search icon background color issue.
* Added search form close icon to close search form.

= 3.0 =
* Added plugin options Search Post Types, Google CSE, Mobile Display and Custom CSS.
* Styled it for default WordPress themes.

= 2.0 =
* Resolved scrolling issue in fixed menu and made magnifying glass icon resizable.

= 1.0 =
* Initial release.

== Upgrade Notice ==

= 4.0 =
* This is a major update.
* Renamed plugin from "Add Search To Menu" to "Ivory Search".
* Changed old settings page from Settings -> Add Search To Menu to Ivory Search -> Settings
* Developed lot of options to enhance search functionality.

