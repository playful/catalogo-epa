		<footer class="footer-principal">
			
			<div class="container">
		    	<div class="row text-left">
		    		<div class="col-xs-10 col-sm-6 col-xs-offset-1 col-sm-offset-0">
		    			<div class="row">
		    				<div class="col-xs-12 col-sm-6 footer-left-text">
		    					<a href="http://www.epa.biz:81/ve/quienes-somos/"> <p>¿Quiénes somos?</p> </a>
		    					<a href="http://www.epa.biz/ve/tiendas/" target="_blank"><p>Nuestras tiendas</p> </a>
		    					<a href="http://www.epa.biz:81/ve/folleto/" target="_blank"><p>Folleto</p> </a>
		    					<a href="http://www.epa.biz:81/ve/prensa/" target="_blank"><p>Prensa</p> </a>
		    				</div>
		    				<div class="col-xs-12 col-sm-6 footer-left-text">
		    					<a href="http://www.epa.biz:81/ve/proveedores/" target="_blank"><p>Aliados comerciales</p> </a>
		    					<a href="http://www.epa.biz:81/ve/servicios/" target="_blank"><p>Servicios</p> </a>
		    				</div>
		    			</div>
		    		</div>
		    		<div class="col-xs-12 col-sm-6 footer-newsletter">
		    			<div class="row">
		    				<div class="col-xs-10 col-sm-8  col-xs-offset-1 col-sm-offset-4 footer-right-text">
		    					<p class="big-text">Infórmese</p>
								<p class="novedades-text">de nuestros eventos, promociones y más</p>
								<?php echo do_shortcode( '[contact-form-7 id="17964" title="Contacto newsletter-footer"]' ); ?>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
	    	</div>
	    </footer>
	
	   <!-- JavaScripts -->

	  <!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script> -->
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	  
	  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/widget-head.js"></script>


	<?= wp_footer(); ?>


    </body>
</html>